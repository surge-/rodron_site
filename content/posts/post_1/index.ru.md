---
weight: 4
title: "Диссертации в области БАС"
date: 2023-09-21
lastmod: 2023-09-22
draft: false
authors: ["sbryukov","apimenov"]
description: "Подборка интересных дисертаций в области безпилотников."
featuredImage: "phd_post.png"

tags: ["статьи" ]
categories: ["PhD"]

lightgallery: true
---

Список кандидатских работ выполненных за последние 5 лет. Интерес представляли как отечественные, так и зарубежные работы на момент осени 2023г. 

<!--more-->

| Название работы | Год защиты | Коментарий |
| ------ | ---- |----------|
| [Адаптивный пилотажно-навигационный индикатор бортовой эргатической системы управления летательного аппарата](https://www.dissercat.com/content/adaptivnyi-pilotazhno-navigatsionnyi-indikator-bortovoi-ergaticheskoi-sistemy-upravleniya)   | 2022, 05.13.05 | Адаптивное формирование информационного контента на пенеле упраления.  |
|[Метод и алгоритмы назначения заданий в распределенной информационной системе Интернета вещей](https://www.dissercat.com/content/metod-i-algoritmy-naznacheniya-zadanii-v-raspredelennoi-informatsionnoi-sisteme-interneta)| 2022, 05.13.11 | Грид-вычисления на флоте IoT устройств. Как идея - распределенные вычисления роем БАС. |
|[Алгоритмическое и программное обеспечение адаптивной системы управления модульными роботами](https://www.dissercat.com/content/algoritmicheskoe-i-programmnoe-obespechenie-adaptivnoi-sistemy-upravleniya-modulnymi-robotam)| 2022, 05.13.11 | Применение линейной алгебры, ТАУ и теории графов в задачах адаптивного управления для «манипулятор» и «шагающая платформа» |
|[Анализ конфигураций модульных вычислительных систем для проверки выполнения ограничений реального времени](https://www.dissercat.com/content/analiz-konfiguratsii-modulnykh-vychislitelnykh-sistem-dlya-proverki-vypolneniya-ogranichenii)| 2022, 05.13.11 | В современных летательных аппаратах модульная архитектура. Разработан метод проверки выполнения ограничений реального времени для заданной конфигурации МВС (количество и типы вычислителей; набор разделов, в т.ч. характеристики входящих в них прикладных задач и зависимости по данным между ними), основанный на конкретизации обобщенной модели МВС для заданной конфигурации МВС. |
|[Масштабируемые алгоритмы одновременного построения карты и локализации стаи мобильных роботов](https://www.dissercat.com/content/masshtabiruemye-algoritmy-odnovremennogo-postroeniya-karty-i-lokalizatsii-stai-mobilnykh-rob)| 2022, 05.13.11 | Для обеспечения точной локализации применяются алгоритмы, решающие задачу SLAM, целью данной работы является разработка масштабируемых алгоритмов для многоагентного решения задачи SLAM, применимых для роботов с ограниченными вычислительными ресурсами. |
|[Методы и алгоритмы планирования транспортных операций гетерогенной группы автономных роботов в условиях пространственно-ситуационной неопределенности](https://www.dissercat.com/content/metody-i-algoritmy-planirovaniya-transportnykh-operatsii-geterogennoi-gruppy-avtonomnykh-rob)| 2022, 05.13.11 | 1. метод синтеза траектории для циклической транспортной задачи 2. метод согласования траекторий 3. метод разрешения коллизий 4. алгоритм поиска траектории движения на многослойной карте |
|[Модели, алгоритмы, программные средства информационного и физического взаимодействия устройств модульной робототехнической системы](https://www.dissercat.com/content/modeli-algoritmy-programmnye-sredstva-informatsionnogo-i-fizicheskogo-vzaimodeistviya-ustroi)| 2021, 05.13.11 | Для компьютерного моделирования робототехнических устройств применялись системы автоматизированного проектирования, такие как Kомпас-3D, Solidworks пакеты и высокоуровневые языки для технических расчетов, а также среды для анализа данных Gazebo и V-Rep. |
|[Методы и средства создания параллельно-конвейерных программ для решения задач реального времени на реконфигурируемых вычислительных системах](https://www.dissercat.com/content/metody-i-sredstva-sozdaniya-parallelno-konveiernykh-programm-dlya-resheniya-zadach-realnogo)| 2020 | Параллельно-конвейерные программы. Вычисления в реальном времени. FPGS, GPU, CPU.|
|[Повышение тестопригодности цифровых электронных модулей бортовых систем управления](https://www.dissercat.com/content/povyshenie-testoprigodnosti-tsifrovykh-elektronnykh-modulei-bortovykh-sistem-upravleniya-0) |2021 | Тестирование модуле на основе BGA микросхем. Система тестирования с исеользованием JTAG. |
|[Модель, метод и нейросетевое оптико-электронное вычислительное устройство распознавания изображений](https://www.dissercat.com/content/model-metod-i-neirosetevoe-optiko-elektronnoe-vychislitelnoe-ustroistvo-raspoznavaniya-izobr) |2020 | ИИ. Обработка изображений. Object detection, Classification. Обратная связь с полетным контроллером. | 
| [Метод, алгоритм и устройство дефаззификации для системы управления ориентацией мобильного робота](https://www.dissercat.com/content/metod-algoritm-i-ustroistvo-defazzifikatsii-dlya-sistemy-upravleniya-orientatsiei-mobilnogo-)| 2018  | Примение FPGA. Оптимизация алгоритмов нечеткой логики. |
|[Алгоритмы коррекции с повышенными характеристиками наблюдаемости и управляемости для навигационного комплекса летательных аппаратов авианосного базирования](https://www.dissercat.com/content/algoritmy-korrektsii-s-povyshennymi-kharakteristikami-nablyudaemosti-i-upravlyaemosti-dlya) | 2022,  05.13.01 | Автаматическа корректция траектории. |
| [Алгоритмы траекторного управления квадрокоптером с несимметричными компоновками и подвешенным грузом для обеспечения равновесного режима полета](https://www.dissercat.com/content/algoritmy-traektornogo-upravleniya-kvadrokopterom-s-nesimmetrichnymi-komponovkami-i-podveshe) | 2022, 05.13.01 | Управление БАС с подвешенным грузом. |
| [Разработка и исследование модели управления легкими дронами в условиях ветровой нагрузки](https://www.dissercat.com/content/razrabotka-i-issledovanie-modeli-upravleniya-legkimi-dronami-v-usloviyakh-vetrovoi-nagruzki) | 2022, 05.13.01 | Делали в МИЭТ, головное предприятие Тульский универ, Лупин Сергей Андреевич МПСУ |
|[Алгоритм управления малым космическим аппаратом технологического назначения для создания благоприятных условий по микроускорениям](https://www.dissercat.com/content/algoritm-upravleniya-malym-kosmicheskim-apparatom-tekhnologicheskogo-naznacheniya-dlya-sozda)  | 2021, 05.13.01 | Много всякой космической математики, в том числе про деформации (наверное актуально для больших дронов) |
|[Алгоритмы коррекции и комплексирования навигационных систем высокоточных беспилотных летательных аппаратов](https://www.dissercat.com/content/algoritmy-korrektsii-i-kompleksirovaniya-navigatsionnykh-sistem-vysokotochnykh-bespilotnykh) | 2021, 05.13.01 | В качестве основного объекта исследования в диссертации рассматриваются НК включающие ИНС, СНС, АИС, РЛС и алгоритмы обработки информации, применяемые в высокоточных БЛА. Но Matlab... |
| [Алгоритмы обработки информации для автономной навигации и планирование маршрута движения планетохода](https://www.dissercat.com/content/algoritmy-obrabotki-informatsii-dlya-avtonomnoi-navigatsii-i-planirovaniya-marshruta-dvizhen)| 2021, 05.13.01 | SLAM, ROS, Gazebo, Matlab, C++ и китайцы. |
|[Алгоритмы фильтрации параметров движения группы беспилотных летательных аппаратов](https://www.dissercat.com/content/algoritmy-filtratsii-parametrov-dvizheniya-gruppy-bespilotnykh-letatelnykh-apparatov) | 2021, 05.13.01 | Для общего понимания проблемы, пример группы БЛА с цифрами. |
|[Верифицируемые системы виртуального моделирования беспилотных транспортных средств](https://www.dissercat.com/content/verifitsiruemye-sistemy-virtualnogo-modelirovaniya-bespilotnykh-transportnykh-sredstv) | 2021, 05.13.01 | Про ADAS. Интересен набор инструментов: Gazebo 10.0, MatLAB 2019b, DART (Dynamic Animation and Robotics Toolkit), ESI Pro-SiVIC 2018. |
|[Исследование методов координации исполнителей в системе управления транспортными роботами](https://www.dissercat.com/content/issledovanie-metodov-koordinatsii-ispolnitelei-v-sisteme-upravleniya-transportnymi-robotami)| 2021, 05.13.01 | Делали в МИЭТ, головное предприятие Тульский универ, Лупин Сергей Андреевич МПСУ, среда моделирования AnyLogic |
|[Модели и алгоритмы управления движением бортового манипулятора с сохранением устойчивости мультиротора в режиме зависания](https://www.dissercat.com/content/modeli-i-algoritmy-upravleniya-dvizheniem-bortovogo-manipulyatora-s-sokhraneniem-ustoichivos)| 2021, 05.13.01 | Модель, алгоритмы, патент, интересно в качестве контекста к тематике |
|[Нечеткие алгоритмы настройки, фильтрации, анализа и синтеза систем управления и навигации](https://www.dissercat.com/content/nechetkie-algoritmy-nastroiki-filtratsii-analiza-i-sinteza-sistem-upravleniya-i-navigatsii)| 2021, 05.13.01 | Баумана, MATLAB, контекст тематики БПЛА |
|[Разработка и исследование алгоритма гарантирующего управления траекторией беспилотного летательного аппарата на основе игрового подхода](https://www.dissercat.com/content/razrabotka-i-issledovanie-algoritma-garantiruyushchego-upravleniya-traektoriei-bespilotnogo)| 2021, 05.13.01 | Алгоритм, методы для того чтобы с помощью БЛА сбить воздушную цель, а самим уклониться |
|[Разработка информационной системы для расчета взлетно-посадочных характеристик воздушных судов на базе электронного планшета пилота](https://www.dissercat.com/content/razrabotka-informatsionnoi-sistemy-dlya-rascheta-vzletno-posadochnykh-kharakteristik-vozdush)| 2021, 05.13.01 | Расчет взлетно-посадочных характеристик воздушных судов с применением технологии продукционной экспертной системы; расчетов зависимостей ВПХ, по номограммам представленным в РЛЭ воздушного судна - напоминает предполётную оценку "цифрового двойника" |
|[Разработка методов и алгоритмов для синтеза управления группой динамических объектов](https://www.dissercat.com/content/razrabotka-metodov-i-algoritmov-dlya-sinteza-upravleniya-gruppoi-dinamicheskikh-obektov)| 2021, 05.13.01 | Обучение нейронной сети, реализующей непрерывную динамическую систему стабилизации |
|[Методология построения цифрового двойника научно-технического центра в нефтегазовой отрасли](https://www.dissercat.com/content/metodologiya-postroeniya-tsifrovogo-dvoinika-nauchno-tekhnicheskogo-tsentra-v-neftegazovoi)| 2020, 05.13.01 | Докторская, теория по цифровым двойникам в прикладном аспекте https://disser.spbu.ru/files/2020/disser_krasnov.pdf |
|[Методы и алгоритмы выбора программной архитектуры систем Интернета вещей](https://www.dissercat.com/content/metody-i-algoritmy-vybora-programmnoi-arkhitektury-sistem-interneta-veshchei)| 2020, 05.13.01 | Алгоритм выбора элементов программной архитектуры (шаблонов архитектуры и тактик разработки) |
|[Методы и алгоритмы группового управления беспилотными летательными аппаратами самолетного типа](https://www.dissercat.com/content/metody-i-algoritmy-gruppovogo-upravleniya-bespilotnymi-letatelnymi-apparatami-samoletnogo)| 2021, 05.13.01 | Методы с анализом и моделированием, хороший контекст проблематики. |
|[Модели и алгоритмы автоматизации обслуживания и управления взаимодействием гетерогенных сельскохозяйственных робототехнических комплексов](https://www.dissercat.com/content/modeli-i-algoritmy-avtomatizatsii-obsluzhivaniya-i-upravleniya-vzaimodeistviem-geterogennykh)| 2020, 05.13.01 | Взаимодейтсвие с узлами обслуживания, формирует понимание контекста взаимодействия дрона и наземной системы обслуживания. |
|[Нейросетевая система планирования траекторий для группы мобильных роботов](https://www.dissercat.com/content/neirosetevaya-sistema-planirovaniya-traektorii-dlya-gruppy-mobilnykh-robotov)| 2020, 05.13.01 | Применение нейронной сети Хопфилда. Python |
|[Повышение точности позиционирования беспилотных летательных аппаратов в условиях искажения или подавления навигационного поля GPS/Глонасс](https://www.dissercat.com/content/povyshenie-tochnosti-pozitsionirovaniya-bespilotnykh-letatelnykh-apparatov-v-usloviyakh-iska)| 20200, 05.13.01 | Подстройка под характеристики спутникового сигнала позиционирования |
|[Разработка алгоритмов для распознавания команд речевого интерфейса кабины пилота](https://www.dissercat.com/content/razrabotka-algoritmov-dlya-raspoznavaniya-komand-rechevogo-interfeisa-kabiny-pilota)| 2020, 05.13.01 | Подстройка алгоритмов распознавания под особенности характеристик речи говорящего. |
|[Информационное и алгоритмическое обеспечение систем управления и маневрирования малогабаритных подводных роботов](https://www.dissercat.com/content/informatsionnoe-i-algoritmicheskoe-obespechenie-sistem-upravleniya-i-manevrirovaniya-malogab)| 2019, 05.13.01 | Контекст и специфика подводных роботов. Arduino, Matlab. |
|[Модели и алгоритмы интеллектуальной навигации для систем управления автономных подвижных объектов](https://www.dissercat.com/content/modeli-i-algoritmy-intellektualnoi-navigatsii-dlya-sistem-upravleniya-avtonomnykh-podvizhnyk)| 2018, 05.13.01 | Подавление шума инерциальных датчиков, автономные дроны в городе и в сельском хозяйстве |
|[Разработка адаптивных алгоритмов роевого интеллекта в проектировании и управлении техническими системами](https://www.dissercat.com/content/razrabotka-adaptivnykh-algoritmov-roevogo-intellekta-v-proektirovanii-i-upravlenii-tekhniche)| 2018, 05.13.01 | Теория роевого интеллекта, важный контекст и математика |
|[Редуцированные динамические экспертные системы и алгоритмы повышения отказоустойчивости прицельно-навигационных комплексов летательных аппаратов](https://www.dissercat.com/content/redutsirovannye-dinamicheskie-ekspertnye-sistemy-i-algoritmy-povysheniya-otkazoustoichivosti)| 2018, 05.13.01 | Матмодель учитывающая погрешности датчиков, использование теории функциональных систем. |
|[Траекторное управление движением мобильных роботов относительно подвижных препятствий](https://www.dissercat.com/content/traektornoe-upravlenie-dvizheniem-mobilnykh-robotov-otnositelno-podvizhnykh-prepyatstvii)| 2018, 05.13.01 | Колесные роботы, ROS, Matlab |
|[Автономная система управления полетом квадрокоптера с возможностью облета препятствий и комплексной навигацией](https://www.dissercat.com/content/avtonomnaya-sistema-upravleniya-poletom-kvadrokoptera-s-vozmozhnostyu-obleta-prepyatstvii-i-)| 2018, 05.13.01 | Совершенствование алгоритмов автопилота и SLAM. Solidworks, C++, Matlab. |
|[Разработка алгоритмов комплексирования навигационных систем летательных аппаратов](https://www.dissercat.com/content/razrabotka-algoritmov-kompleksirovaniya-navigatsionnykh-sistem-letatelnykh-apparatov)| 2017, 05.13.01 | Пример применения теории автоматического управления, навигационных систем, интеллектуальных систем, методы системного синтеза и самоорганизации, математического и полунатурного моделирования и программный пакет Matlab. |













