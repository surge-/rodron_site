---
weight: 4
title: "Интересные статьи издания Dron MDPI за 2023г."
date: 2023-09-26
lastmod: 2023-09-26
draft: true
authors: ["sbryukov","apimenov"]
description: "Интересные зарубежные публикации на тему БАС, за 2023г."
featuredImage: "2023-09-26_154622.png"

tags: ["статьи" ]
categories: ["PhD"]

lightgallery: true
---

Статьи журнала [Dron MDPI за 2023г](https://www.mdpi.com/journal/drones). на тему Беспилотные Автономные Средва, привлекшие внимание. Интересовали тренды в теме автопилотных технологий для БАС в околонаучных кругах за рубежем.

<!--more-->
| Название работы | ВУЗ | Коментарий |
| ------ | ---- |----------|
|[YOLO-Based UAV Technology: A Review of the Research and Its Applications](https://www.mdpi.com/2504-446X/7/3/190)|Shenyang Agricultural University, South China Agricultural University| YOLO как самая популярная архитектура NN. Ребята исследую производительность и применимость сети в realtime задачах. YBUT - YOLO based UAV technology. Рассматривают сети: YOLODrone,VIT-YOLO,YOLO-RTUAV,YOLO-Neck, YOLOv7-DeepSORT|
|[MobileRaT: A Lightweight Radio Transformer Method for Automatic Modulation Classification in Drone Communication Systems](https://www.mdpi.com/2504-446X/7/10/596)|Shandong Management University,Xi’an Research Institute of High Technology,University of Pisa,National Institute for Applied Sciences of Rennes|Рассматривается realtime решение для орпеделния типа модуляции коммуникационных каналов дронов с использованием радио-трансформера  MobileRaT, упоминается RadioML.|
|[An Autonomous Soaring for Small Drones Using the Extended Kalman Filter Thermal Updraft Center Prediction Method Based on Ordinary Least Squares](https://www.mdpi.com/2504-446X/7/10/603)|Northwestern Polytechnical University|Дрон парит на восходящем термальном потоке воздуха используя фильтр Калмана.|
| [Research on Scenario Modeling for V-Tail Fixed-Wing UAV Dynamic Obstacle Avoidance](https://www.mdpi.com/2504-446X/7/10/601) | Sun Yat-sen University, Southern Marine Science and Engineering Guangdong Laboratory,Northwestern Polytechnical University, Aviation Industry Corporation of China,| Опыт использования симулятора Gazebo в связке с ROS для симуляции задачи уклонения от столкновений.|
|[Automated Identification and Classification of Plant Species in Heterogeneous Plant Areas Using Unmanned Aerial Vehicle-Collected RGB Images and Transfer Learning](https://www.mdpi.com/2504-446X/7/10/599)|University of Brescia,University of Brescia| Задача классификации, мульти сегминтации растений с помощью NN сетей EfficientNetV2,KNN,ResNet50,Xception, DenseNet121, InceptionV3, и MobileNetV2. Используется Data Set ArcGIS.|
|[Distributed Control for Multi-Robot Interactive Swarming Using Voronoi Partioning](https://www.mdpi.com/2504-446X/7/10/598)|Université Paris-Saclay,||

