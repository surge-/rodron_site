---
weight: 4
title: "Статьи по БПЛА."
date: 2023-10-02
lastmod: 2023-10-02
draft: true
authors: ["sbryukov","apimenov"]
description: "Интересные публикации на тему БАС, в русскоязычных журналах."
featuredImage: "kiberleninka.jpg"

tags: ["статьи" ]
categories: ["PhD"]

lightgallery: true
---

Статьи в журналах из списка ВАК и РНИЦ и т.д. в поисковой системе  [КиберЛенинка с 2019г.](https://cyberleninka.ru/). на тему Беспилотные Автономные Средва, привлекшие внимание. Интересовали тренды в теме автопилотных технологий для БАС в околонаучных кругах.

<!--more-->
| Название работы | ВУЗ | Коментарий |
| ------ | ---- |----------|
|[Анализ и перспективы развития беспилотных летательных аппаратов](https://cyberleninka.ru/article/n/analiz-i-perspektivy-razvitiya-bespilotnyh-letatelnyh-apparatov)| Просвирнина Наталья, МАИ, 2021| |
|[Стратегии управления роем беспилотных летательных аппаратов](https://cyberleninka.ru/article/n/strategii-upravleniya-roem-bespilotnyh-letatelnyh-apparatov)| Мустафаев А.Ф., УГТУ, 2019 | |
|[Искусственный интеллект в беспилотных авиационных системах](https://cyberleninka.ru/article/n/iskusstvennyy-intellekt-v-bespilotnyh-aviatsionnyh-sistemah)| Матюха С.В., ООО "Аэтосвет", 2022 | |
|[Анализ и оптимизация алгоритмов управления полетом беспилотных летательных аппаратов (бпла) с помощью машинного обучения](https://cyberleninka.ru/article/n/analiz-i-optimizatsiya-algoritmov-upravleniya-poletom-bespilotnyh-letatelnyh-apparatov-bpla-s-pomoschyu-mashinnogo-obucheniya)| Негрий А.В. Соколов О.А., СПбГУ ГА, 2023 | |
|[К вопросу о применении беспилотных пожарных летательных аппаратов (бпла или дронов) в чрезвычайных ситуациях](https://cyberleninka.ru/article/n/k-voprosu-o-primenenii-bespilotnyh-pozharnyh-letatelnyh-apparatov-bpla-ili-dronov-v-chrezvychaynyh-situatsiyah)| Деева Анна Сергеевна Аксенов Сергей Геннадьевич, УУНиТ, 2022 | |
|[Применение беспилотных летательных аппаратов в строительстве](https://cyberleninka.ru/article/n/primenenie-bespilotnyh-letatelnyh-apparatov-v-stroitelstve)| Кудасова А.С. Тютина А.Д. Сокольникова Э.В., ДГТУ, 2022 | |