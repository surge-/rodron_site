---
weight: 4
title: "Cтатьи издания журнала MDPI Dron за Сентябрь 2023г."
date: 2023-10-04
lastmod: 2023-10-04
draft: true
authors: ["sbryukov","apimenov"]
description: "Обзор выпуска журнал MDPI, Drones за Сентяьрь 2023г."
featuredImage: "banner.png"

tags: ["статьи" ]
categories: ["PhD"]

lightgallery: true
---

Интересные статьи из  журнала [MDPI Drones за Сентябрь  2023г](https://www.mdpi.com/2504-446X/7/9). Тема номера - использование термальных потоков в автопилотируемых полетах с предсказанием паметров потоков воздуха и ветра.

<!--more-->
| Название работы | ВУЗ | Коментарий |
| ------ | ---- |----------|
|[Cooperative Standoff Target Tracking using Multiple Fixed-Wing UAVs with Input Constraints in Unknown Wind](https://www.mdpi.com/2504-446X/7/9/593)|School of Computer Science, Huanggang Normal University, ChangJiang Industry Investment Group CO|  Группа барражирующих дронов вокруг движущейся цели, при этом учитывается не предсказуемость порывов ветра. Используеются опорные точки, вектора-функуии Ляпунова. |
|[Learning Template-Constraint Real-Time Siamese Tracker for Drone AI Devices via Concatenation](https://www.mdpi.com/2504-446X/7/9/592)|School of Information and Software Engineering, University of Electronic Science and Technology of China| Трэкикнг способом сеамские сети на базе MobileNetV2. Метод ContactTrk на основе упащенной архитектуры нейросети. Достигнуто 41 FPS на Nvidia AGX Xavier. Ссылки на бенчмарки [uav123](https://cemse.kaust.edu.sa/ivul/uav123),[OTB100](https://paperswithcode.com/sota/visual-object-tracking-on-otb-100), [LaSOT](https://paperswithcode.com/dataset/lasot) |
|[Optimization of Full-Duplex UAV Secure Communication with the Aid of RIS](https://www.mdpi.com/2504-446X/7/9/591)|Chengdu University of Technology,Southwest University, Chongqing||
|[Joint Resource Allocation and Drones Relay Selection for Large-Scale D2D Communication Underlaying Hybrid VLC/RF IoT Systems](https://www.mdpi.com/2504-446X/7/9/589)|Beijing Electronics Science and Technology Institute, Beijing University of Posts and Telecommunications,Beijing University of Posts and Telecommunications,Kashi University| Системы D2D коммуникации на основе IoT для большних групп дронов. Построние line-of-sight(LoS) коммуникации. Вводится понятие MARL(multi-agent reinforcement learning) решающе задачу Пулемётное восхождение (hill-climbing)|
|[A Low-Altitude Obstacle Avoidance Method for UAVs Based on Polyhedral Flight Corridor](https://www.mdpi.com/2504-446X/7/9/588)||Решение для анти-столкновения для дронов на малой высоте на основе многогранного полетного корридора. |
|[SkyroadAR: An Augmented Reality System for UAVs Low-Altitude Public Air Route Visualization](https://www.mdpi.com/2504-446X/7/9/587)||Одображени и трэкинг дрона в AR пространсве используя сенсоры и камеру дрона.|
|[Estimating Maize Maturity by Using UAV Multi-Spectral Images Combined with a CCC-Based Model](https://www.mdpi.com/2504-446X/7/9/586)||Прикладная ИИ задача. Оценки спелости кукурузы дроном с мультиспектральной камерой.|
|[Drone Based RGBT Tracking with Dual-Feature Aggregation Network](https://www.mdpi.com/2504-446X/7/9/585)||Реализация трэкинга объекта для мульти-модальнного сенсора: видимая и тепловая камеры. [RGB-Termal tracking](https://paperswithcode.com/task/rgb-t-tracking) [DataSet VTUAV](https://zhang-pengyu.github.io/DUT-VTUAV/)|
|[Challenges in Inter-UAV 60 GHz Wireless Communication Utilizing Instantaneous Proximity Opportunities in Flight](https://www.mdpi.com/2504-446X/7/9/583)|| D2D коммуникация на mmWave частотах. 60GHz|
|[An Operational Capacity Assessment Method for an Urban Low-Altitude Unmanned Aerial Vehicle Logistics Route Network](https://www.mdpi.com/2504-446X/7/9/582)||О функционированни дрона в рамках USА национального стандарта регулирущего движени дронов. [Urban Air Mobility (UAM)](https://www.faa.gov/air-taxis/uam_blueprint). О определении максимально возможном кол-ве дронов в выделенной трассе.|
|[Mapping the Urban Environments of Aedes aegypti Using Drone Technology](https://www.mdpi.com/2504-446X/7/9/581)|||
|[Development of Multimode Flight Transition Strategy for Tilt-Rotor VTOL UAVs](https://www.mdpi.com/2504-446X/7/9/580)|||
|[Hierarchical Matching Algorithm for Relay Selection in MEC-Aided Ultra-Dense UAV Networks](https://www.mdpi.com/2504-446X/7/9/579)|||
|[Comparison of Machine Learning Pixel-Based Classifiers for Detecting Archaeological Ceramics](https://www.mdpi.com/2504-446X/7/9/578)|||
|[A Single-Anchor Cooperative Positioning Method Based on Optimized Inertial Measurement for UAVs](https://www.mdpi.com/2504-446X/7/9/577)|||
|[A Collaborative Inference Algorithm in Low-Earth-Orbit Satellite Network for Unmanned Aerial Vehicle](https://www.mdpi.com/2504-446X/7/9/575)|||
|[SmrtSwarm: A Novel Swarming Model for Real-World Environments](https://www.mdpi.com/2504-446X/7/9/573)|||
|[Multiple Unmanned Aerial Vehicle Autonomous Path Planning Algorithm Based on Whale-Inspired Deep Q-Network](https://www.mdpi.com/2504-446X/7/9/572)|||
|[A Visual Odometry Pipeline for Real-Time UAS Geopositioning](https://www.mdpi.com/2504-446X/7/9/569)|||
|[Reinforcement Learning-Based Low-Altitude Path Planning for UAS Swarm in Diverse Threat Environments](https://www.mdpi.com/2504-446X/7/9/567)|||
|[Data Fusion Analysis and Synthesis Framework for Improving Disaster Situation Awareness](https://www.mdpi.com/2504-446X/7/9/565)|||
|[Go with the Flow: Estimating Wind Using Uncrewed Aircraft](https://www.mdpi.com/2504-446X/7/9/564)|||
|[Constraint Programming Approach to Coverage-Path Planning for Autonomous Multi-UAV Infrastructure Inspection](https://www.mdpi.com/2504-446X/7/9/563)|||
|[UAV Digital Twin Based Wireless Channel Modeling for 6G Green IoT](https://www.mdpi.com/2504-446X/7/9/562)|||
|[Hierarchical Task Assignment for Multi-UAV System in Large-Scale Group-to-Group Interception Scenarios](https://www.mdpi.com/2504-446X/7/9/560)|||
|[2chADCNN: A Template Matching Network for Season-Changing UAV Aerial Images and Satellite Imagery](https://www.mdpi.com/2504-446X/7/9/558)|||
|[A Robust Disturbance-Rejection Controller Using Model Predictive Control for Quadrotor UAV in Tracking Aggressive Trajectory](https://www.mdpi.com/2504-446X/7/9/557)|||
|[A Safety-Assured Semantic Map for an Unstructured Terrain Environment towards Autonomous Engineering Vehicles](https://www.mdpi.com/2504-446X/7/9/550)|||
|[Stepwise Soft Actor–Critic for UAV Autonomous Flight Control](https://www.mdpi.com/2504-446X/7/9/549)|||
|[Validation of the Flight Dynamics Engine of the X-Plane Simulator in Comparison with the Real Flight Data of the Quadrotor UAV Using CIFER](https://www.mdpi.com/2504-446X/7/9/548)|||
|[Multi-UAV Cooperative and Continuous Path Planning for High-Resolution 3D Scene Reconstruction](https://www.mdpi.com/2504-446X/7/9/544)|||
