---
weight: 4
title: "UAV digital twin"
date: 2023-10-05
lastmod: 2023-10-05
draft: true
authors: ["sbryukov","apimenov"]
description: "Обзор публикаций и доступного софта 'ифрогового двойника' дрона"
featuredImage: "uav-digital-twin-vehicle.png"

tags: ["статьи" ]
categories: ["PhD"]

lightgallery: true
---

Подборка информации по "цифровому двойнику" дронов.

<!--more-->
| Название работы | ВУЗ | Коментарий |
| ------ | ---- |----------|
|[Development of a Predictive Digital Twin](https://kiwi.oden.utexas.edu/research/digital-twin)|Funded by AFOSR Dynamic Data–Driven Application Systems (DDDAS) · Program Officer Dr. Erik Blasch · MIT-SUTD International Design Centre · The Boeing Company · AFOSR Computational Mathematics · Program Officer Dr. Fariba Fahroo||
|[Digital Twin Development for the Airspace of the Future](https://doi.org/10.3390/drones7070484)|School of Aerospace, Transport and Manufacturing, Cranfield University, Cranfield MK43 0AL, UK | |
|[The study and development of UAV digital twin system](https://iopscience.iop.org/article/10.1088/1742-6596/2366/1/012038)|Jianfeng Yang, Chuangmian Huang and Jingru Wang||
|[Research on Digital Twin Framework of Military Large-scale UAV Based on Cloud Computing](https://iopscience.iop.org/article/10.1088/1742-6596/1738/1/012052)|Yin-chuan Wang, Na Zhang, Hongshun Li and Junjie Cao||
|[DTUAV: a novel cloud–based digital twin system for unmanned aerial vehicles](https://www.researchgate.net/publication/362421134_DTUAV_a_novel_cloud-based_digital_twin_system_for_unmanned_aerial_vehicles)|Wei Meng (GuangDong University of Technology), Yuanlin Yang, Jiayao Zang, Hongyi Li| [DTUAV](https://github.com/DTUAV/DTUAV) |
|[Simio Simulation Software can be used for Digital Twin where a virtual copy](https://www.simio.com/applications/industry-40/Digital-Twin-Software.php#:~:text=Simio%20Simulation%20Software%20can%20be,the%20components%20of%20the%20entity.)|https://www.simio.com/| Пример программной реализации "цифрового двойника" дрона |
|[UAV Digital Twin via Probabilistic Graphical Models and ROS](https://github.com/michaelkapteyn/UAV-Digital-Twin)| Michael Kapteyn, PhD student Aerospace Computational Design Lab (ACDL) | ROS 2 packages that implement dynamic structural health monitoring for a UAV |
