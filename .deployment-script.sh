#!/bin/bash

# SSH to server and git pull from prod branch
mkdir -p ~/.ssh
echo -e "$CICD_RSA" > ~/.ssh/id_rsa
echo -e "$CICD_RSA_PUB" > ~/.ssh/id_rsa.pub
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub

ssh -t -t -o StrictHostKeyChecking=no ubuntu@185.251.91.179 "cd /home/ubuntu/rodron.rf/ && git pull origin main"




